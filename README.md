# Simple todo app with vanilla javascript

## Project setup
```
npm install or yarn
```

### Compiles and hot-reloads for development
```
npm run serve or yarn serve
```

### Compiles and minifies for production
```
npm run build or yarn build
```

### Customize configuration
See [Tailwindcss](https://tailwindcss.com/docs/)  docs.

See [Parcel Bundler](https://parceljs.org/getting_started.html) docs.