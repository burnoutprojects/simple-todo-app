import "../css/main.css";

var todos = [];

const todoInput = document.querySelector("input[name=todoInput]");
const addBtn = document.querySelector("button[name=todoBtn]");
const error = document.querySelector("span.error");
const uList = document.querySelector("ul.uncompleted");
const cList = document.querySelector("ul.completed");

/**
 *
 * HELPERS
 *
 */

/**
 * Load the todos from localStorage
 * @param key Key of localStorage item (default: todos)
 */
const loadTodos = (key = "todos") =>
  (todos = localStorage.getItem(key)
    ? JSON.parse(localStorage.getItem(key))
    : []);

/**
 * Save the todos from localStorage
 * @param key Key of localStorage item (default: todos)
 */
const saveTodos = (key = "todos") =>
  localStorage.setItem(key, JSON.stringify(todos));

/**
 * Move the cursor to end of an input element or any other element with contenteditable="true"
 */
const moveCursorToEnd = el => {
  const range = document.createRange();
  const sel = window.getSelection();
  range.selectNodeContents(el);
  range.collapse(false);
  sel.removeAllRanges();
  sel.addRange(range);
  el.focus();
  range.detach();
};

/**
 * Return the todo item index
 * @param {String} id Todo ID
 * @returns {Number}
 */
const getTodoIndex = id => todos.findIndex(todo => todo.id === id);

/**
 * Return a random ID
 * @returns {String}
 */
const getRandomId = () =>
  Math.random()
    .toString(36)
    .substr(2, 5);

/**
 * Append a todo item to dom
 * @param {Object} todo A todo object
 */
const todoListEl = todo => {
  const el = `
  <li class="flex items-center justify-center" data-id="${todo.id}">
    <div class="flex items-center w-10/12">
      <div class="mx-1">
        <input type="checkbox" ${todo.completed ? "checked" : ""} />
      </div>
      <p class="truncate w-full mr-1 ${todo.completed ? "line-through" : ""}">
        ${todo.body}
      </p>
    </div>
    <div
      class="flex flex-col items-center justify-center w-2/12 mr-1"
    >
      <button class="edit border rounded w-full my-1 py-1">
        Edit
      </button>
      <button class="delete border rounded w-full my-1 py-1 ${
        todo.completed ? "hidden" : ""
      }">
        Delete
      </button>
    </div>
  </li>
  `;

  if (!todo.completed) {
    uList.insertAdjacentHTML("afterbegin", el);
  } else {
    cList.insertAdjacentHTML("afterbegin", el);
  }
};

/**
 * Check if the lists are empty and toggle empty list placeholder
 */
const toggleEmptyList = () => {
  if (uList.childElementCount === 1) {
    uList.firstElementChild.classList.remove("hidden");
  } else {
    uList.lastElementChild.classList.add("hidden");
  }
  if (cList.childElementCount === 1) {
    cList.firstElementChild.classList.remove("hidden");
  } else {
    cList.lastElementChild.classList.add("hidden");
  }
};

/**
 *
 * GRUD FUNCTIONS
 *
 */

/**
 * Add a new todo to local storage
 */
const addTodo = () => {
  error.classList.add("hidden");
  const todoBody = todoInput.value.trim();

  if (todoBody === "") {
    error.classList.remove("hidden");
    return;
  }

  const id = getRandomId();

  const todo = {
    id: id,
    body: todoBody,
    completed: false
  };

  todoInput.value = "";
  todos.push(todo);
  todoListEl(todo);
  toggleEmptyList();
  saveTodos();
};

/**
 * Edit a todo and save it to local storage
 * @param  e The firing event
 */
const editTodo = e => {
  console.log(e);
  if (e.target.matches("p[contenteditable]") && e.type === "blur")
    e.target.blur();
  else if (!e.target.matches("button.edit")) return;

  e.preventDefault();

  const listItem = e.target.closest("li");
  const itemText = listItem.querySelector("p");
  const todoId = listItem.dataset.id;
  const index = getTodoIndex(todoId);

  if (itemText.getAttribute("contenteditable")) {
    todos[index].body = itemText.innerText;
    itemText.removeAttribute("contenteditable");
    if (todos[index].completed) {
      itemText.classList.add("line-through");
    }

    saveTodos();
  } else {
    itemText.setAttribute("contenteditable", true);
    itemText.classList.remove("line-through");
    itemText.addEventListener("blur", editTodo);
    moveCursorToEnd(itemText);
  }
};

/**
 * Delete a todo from local storage
 */
const deleteTodo = e => {
  if (!e.target.matches("button.delete")) return;

  const rootList = e.target.closest("ul");
  const listItem = e.target.closest("li");
  const todoId = listItem.dataset.id;
  const index = getTodoIndex(todoId);

  rootList.removeChild(listItem);
  toggleEmptyList();

  todos.splice(index, 1);
  saveTodos();
};

/**
 * Toggle todo item completed status and move it between lists
 */
const toggleCompleted = e => {
  const el = e.target.closest("li");
  const id = el.dataset.id;
  const index = getTodoIndex(id);

  if (uList.contains(el)) {
    todos[index].completed = true;
    el.querySelector("p").classList.add("line-through");
    el.querySelector("input[type=checkbox]").setAttribute("checked", true);
    el.querySelector("button.edit").classList.add("hidden")
    cList.insertBefore(el, cList.firstElementChild);
    toggleEmptyList();
  } else {
    todos[index].completed = false;
    el.querySelector("p").classList.remove("line-through");
    el.querySelector("input[type=checkbox]").removeAttribute("checked");
    el.querySelector("button.edit").classList.remove("hidden")
    uList.insertBefore(el, uList.firstElementChild);
    toggleEmptyList();
  }
  saveTodos();
};

/**
 *
 * EVENT LISTENERS
 *
 */

addBtn.addEventListener("click", addTodo);
document.body.addEventListener("click", editTodo);
document.body.addEventListener("click", deleteTodo);
document.body.addEventListener("change", toggleCompleted);

/**
 *
 * INITIAL
 *
 */

loadTodos();
todos.forEach(todo => {
  todoListEl(todo);
});
toggleEmptyList();
